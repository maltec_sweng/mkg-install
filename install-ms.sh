#! /bin/sh

# Install requirements from package manager
apt update
apt install -y git python3 python3-pip mosquitto nodejs npm make g++ gcc

# Setup mosquitto broker
echo "#############################################################################"
echo "###                         Setup mosquitto broker                        ###"
echo "#############################################################################"
echo "allow_anonymous true" | tee -a /etc/mosquitto/mosquitto.conf
echo "listener 1883" | tee -a /etc/mosquitto/mosquitto.conf
systemctl restart mosquitto

# Install Zigbee2MQTT
echo "#############################################################################"
echo "###                               Zigbee2MQTT                             ###"
echo "#############################################################################"
echo "Get latest version"
git clone https://github.com/Koenkk/zigbee2mqtt.git /opt/zigbee2mqtt
chown -R $SUDO_USER:$SUDO_USER /opt/zigbee2mqtt
cd /opt/zigbee2mqtt
echo "Install with npm"
sudo -u $SUDO_USER npm ci

# Add zigbee2mqtt to systemd
echo "Setup systemd service"
wget -O /etc/systemd/system/zigbee2mqtt.service https://gitlab.com/maltec_sweng/mkg-install/-/raw/main/zigbee2mqtt.service
sed -i "s/USER/$SUDO_USER/g" /etc/systemd/system/zigbee2mqtt.service
systemctl enable zigbee2mqtt
systemctl start zigbee2mqtt

# Install the system controller
echo "#############################################################################"
echo "###                            MKGMS Controller                           ###"
echo "#############################################################################"
echo "Downloading latest version"
git clone https://gitlab.com/maltec_sweng/kg-systemcontroller.git /opt/mkgms
chown -R $SUDO_USER:$SUDO_USER /opt/mkgms
cd /opt/mkgms

echo "Install requirements with pip" 
sudo -u $SUDO_USER pip install -r requirements.txt

# add systemcontroller to systemd
echo "Setup systemd service"
wget -O /etc/systemd/system/mkgms.service https://gitlab.com/maltec_sweng/mkg-install/-/raw/main/mkgms.service
sed -i "s/USER/$SUDO_USER/g" /etc/systemd/system/mkgms.service
systemctl enable mkgms

echo ""
echo "Done"
echo "Please copy /opt/mkgms/config.json.sample to /opt/mkgms/config.json and configure."
echo "Finally run 'systemctl start mkgms' to start the system controller."
echo "You should probably also change '/opt/zigbee2mqtt/config.json' to disable permit_join after pairing your devices."