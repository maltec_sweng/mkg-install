#! /bin/sh

apt update
apt install -y git nodejs npm mariadb-server

echo "#############################################################################"
echo "###             Getting the latest version of the Web Server              ###"
echo "#############################################################################"
git clone https://gitlab.com/maltec_sweng/nodejs-web.git /opt/mkgws
chown -R $SUDO_USER:$SUDO_USER /opt/mkgws
cd /opt/mkgws

echo "#############################################################################"
echo "###                    Setting up the MariaDB database                    ###"
echo "#############################################################################"
echo "Enable and start the MariaDB service"
systemctl enable mariadb.service
systemctl start mariadb.service

#echo "Specify password for webservice database user? Leave blank to generate random password."
#read -p "Password: " password
if [ -z "$password" ]; then
    password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
fi;

echo "Create webservice user"
mariadb -u root -e "CREATE USER 'webservice'@'localhost' IDENTIFIED BY '$password';"
echo "Granting privileges to webservice user"
mariadb -u root -e "GRANT ALL PRIVILEGES ON *.* TO 'webservice'@'localhost' WITH GRANT OPTION;"
echo "Creating database"
mariadb -u root -e "CREATE DATABASE IF NOT EXISTS kitchenguard;"

echo "#############################################################################"
echo "###                      Configuring the web service                      ###"
echo "#############################################################################"
echo "Writing configuration file"
cat << EOF > config/db.json
{
    "host": "localhost",
    "user": "webservice",
    "password": "$password",
    "database": "kitchenguard",
    "timezone": "UTC"
}
EOF

echo "Install npm dependencies"
sudo -u $SUDO_USER npm install

echo "Create service"
wget -O /etc/systemd/system/mkgws.service https://gitlab.com/maltec_sweng/mkg-install/-/raw/main/mkgws.service
sed -i "s/USER/$SUDO_USER/g" /etc/systemd/system/mkgws.service
systemctl enable mkgws
systemctl start mkgws

echo ""
echo "Done. The interface is now available at http://localhost:8080"
echo "Please run 'node setup --new-monitoring-server' to setup the monitoring server."